const mongoose = require('mongoose');

const DB_URI = "mongodb+srv://tasnim:tasnim93@samplecluster.yqzm3.mongodb.net/SampleApi?retryWrites=true&w=majority";

async function connectdb() {
  try {
    await mongoose.connect(DB_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });
  } catch (err) {
    process.exit(1);
  }
}

module.exports = connectdb;