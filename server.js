const express = require('express');
const app = express();
const morgan = require('morgan');
const PORT = 5000;

app
  .use(morgan('short'))
  .use(express.urlencoded({ extended: true }))
  .use(express.json());

// DB connection 
require('./config/db.config')();

// Mount routes
require('./src/app')(app);

app.listen(PORT, () => console.log(`listening on ${PORT}`));