const router = require('express').Router();

const {
  getAllProducts,
  getOneProduct,
  createOneProduct,
  updateOneProduct,
  deleteOneProduct
} = require('../controller/data.controller');

/**
* @api {GET} /api/product/ Get all products
* @apiGroup User
* @apiPermission All
*
* @apiSuccess (200) {Object} `Products` object
 */
router.get('/', getAllProducts);

/**
* @api {GET} /api/product/:id Get single product
* @apiGroup User
* @apiPermission All
*
* @apiSuccess (200) {Object} `Product` object
 */
router.get('/:id', getOneProduct);

/**
* @api {POST} /api/product/ Create single product
* @apiGroup User
* @apiPermission All
*
* @apiSuccess (200) {Object} `Product` object
 */
router.post('/', createOneProduct);


/**
* @api {PUT} /api/product/:id Update product by productId
* @apiGroup User
* @apiPermission All
*
* @apiSuccess (200) {Object} `Product` object
 */
router.put('/:id', updateOneProduct);


/**
* @api {DELETE} /api/product/:id Delete product by id
* @apiGroup User
* @apiPermission All
*
* @apiSuccess (200) {Object} `Null` object
 */
router.delete('/:id', deleteOneProduct);


module.exports = router;