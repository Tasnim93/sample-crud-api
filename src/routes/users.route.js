const router = require('express').Router();

const {
  getAllUsers,
  getOneUser,
  createOneUser,
  updateOneUser,
  deleteOneUser
} = require('../controller/data.controller');

/**
* @api {GET} /api/v1/ Get all users
* @apiGroup User
* @apiPermission All
*
* @apiSuccess (200) {Object} `Users` object
 */
router.get('/', getAllUsers);

/**
* @api {GET} /api/v1/:id Get single user
* @apiGroup User
* @apiPermission All
*
* @apiSuccess (200) {Object} `User` object
 */
router.get('/:id', getOneUser);

/**
* @api {POST} /api/v1/ Create user
* @apiGroup User
* @apiPermission All
*
* @apiSuccess (200) {Object} `User` object
 */
router.post('/', createOneUser);


/**
* @api {PUT} /api/v1/:id Update user by userId
* @apiGroup User
* @apiPermission All
*
* @apiSuccess (200) {Object} `User` object
 */
router.put('/:id', updateOneUser);


/**
* @api {DELETE} /api/v1/:id Delete user by id
* @apiGroup User
* @apiPermission All
*
* @apiSuccess (200) {Object} `Null` object
 */
router.delete('/:id', deleteOneUser);


module.exports = router;