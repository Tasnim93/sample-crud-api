const User = require('../model/user.model');
const Product = require('../model/product.model');
const DataService = require('../services/data.service');

// CRUD functionality for User Model
exports.getAllUsers = DataService.getAll(User);
exports.getOneUser = DataService.getOne(User);
exports.createOneUser = DataService.createOne(User);
exports.updateOneUser = DataService.updateOne(User);
exports.deleteOneUser = DataService.deleteOne(User);

// CRUD functionality for Product Model
exports.getAllProducts = DataService.getAll(Product);
exports.getOneProduct = DataService.getOne(Product);
exports.createOneProduct = DataService.createOne(Product);
exports.updateOneProduct = DataService.updateOne(Product);
exports.deleteOneProduct = DataService.deleteOne(Product);