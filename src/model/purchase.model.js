const mongoose = require('mongoose');

const purchaseSchema = new mongoose.Schema({
  user: { type: String, required: true },
  product: { type: String, required: true },
  date: { type: Date, default: Date.now() },
  delivered: { type: Boolean, required: true }
});

module.exports = mongoose.model('Purchase', purchaseSchema);