const mongoose = require('mongoose');
const shortid = require('shortid');

const creaditSchema = new mongoose.Schema({
  creditId: { type: String, default: shortid.generate },
  value: { type: String, required: true },
  fromDate: { type: String, required: true },
  toDate: { type: String, required: true }
});

module.exports = mongoose.model('Credit', creaditSchema);