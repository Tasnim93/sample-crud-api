const mongoose = require('mongoose');
const shortid = require('shortid');

const userSchema = new mongoose.Schema({
  userId: { type: String, default: shortid.generate },
  name: { type: String, required: true },
  surname: { type: String, required: true },
  occupation: { type: String, required: true },
  email: { type: String, required: true }
});

module.exports = mongoose.model('User', userSchema);