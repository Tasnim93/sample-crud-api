const mongoose = require('mongoose');
const shortid = require('shortid');

const productSchema = new mongoose.Schema({
  productId: { type: String, default: shortid.generate },
  name: { type: String, required: true },
  company: { type: String, required: true },
  category: { type: String, required: true },
  price: { type: Number, required: true }
});

module.exports = mongoose.model('Product', productSchema);