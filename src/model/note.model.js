const mongoose = require('mongoose');

const noteSchema = new mongoose.Schema({
  noteId: { type: String, required: true },
  user: { type: String, required: true },
  text: { type: String, default: Date.now() },
});

module.exports = mongoose.model('Note', noteSchema);