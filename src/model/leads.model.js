const mongoose = require('mongoose');
const shortid = require('shortid');

const leadschema = new mongoose.Schema({
  leadId: { type: String, default: shortid.generate },
  product: { type: String, required: true },
  email: { type: String, required: true },
  date: { type: Date, default: Date.now() },
});

module.exports = mongoose.model('Lead', leadschema);