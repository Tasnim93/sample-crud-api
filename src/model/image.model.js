const mongoose = require('mongoose');
const shortid = require('shortid');

const imageSchema = new mongoose.Schema({
  imageId: { type: String, default: shortid.generate },
  product: { type: String, required: true },
  angle: { type: Date, default: Date.now() },
  type: { type: String, required: true }
});

module.exports = mongoose.model('Image', imageSchema);