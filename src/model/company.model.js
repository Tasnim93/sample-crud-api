const mongoose = require('mongoose');
const shortid = require('shortid');

const companySchema = new mongoose.Schema({
  companyId: { type: String, default: shortid.generate },
  name: { type: String, required: true },
  status: { type: String, default: 'active' },
  affiliate: { type: Boolean, default: false },
});

module.exports = mongoose.model('Company', companySchema);