const userRoute = require('./routes/users.route');
const productRoute = require('./routes/products.route');

function initRoutes(app) {
  app.use('/api/user', userRoute)
    .use('/api/product', productRoute);
}

module.exports = initRoutes;