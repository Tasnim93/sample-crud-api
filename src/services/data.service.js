
// @method  GET
// @desc    Get all documents
exports.getAll = Model => async (req, res) => {
  try {
    const items = await Model.find();

    return res.status(200).json({
      success: true,
      data: items
    });
  } catch (err) {
    // Error handler can be implemented later on
    console.log(err.message);
  }
};


// @method  GET
// @desc    Get single document
exports.getOne = Model => async (req, res) => {
  try {
    const item = await Model.findById({ _id: req.params.id });

    return res.status(200).json({
      success: true,
      data: item
    });
  } catch (err) {
    // Error handler can be implemented later on
    console.log(err.message);
  }
};


// @method  POST
// @desc    Create single document
exports.createOne = Model => async (req, res) => {
  try {
    const item = await Model.create(req.body);

    return res.status(200).json({
      success: true,
      data: item
    });
  } catch (err) {
    // Error handler can be implemented later on
    console.log(err.message);
  }
};


// @method  PUT
// @desc    Update fields on single documents
exports.updateOne = Model => async (req, res) => {
  try {
    const item = await Model.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true
    });

    return res.status(200).json({
      success: true,
      data: item
    });
  } catch (err) {
    // Error handler can be implemented later on
    console.log(err.message);
  }
};


// @method  DELETE
// @desc    Delete single document
exports.deleteOne = Model => async (req, res) => {
  try {
    await Model.findByIdAndDelete(req.params.id);

    return res.status(200).json({
      success: true,
      data: null
    });
  } catch (err) {
    // Error handler can be implemented later on
    console.log(err.message);
  }
};